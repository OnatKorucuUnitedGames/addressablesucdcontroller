﻿using AddressablesUCDController.Models;
using AddressablesUCDController.Enums;
using System.Text;

namespace AddressablesUCDController.Controllers
{
    public class UcdConfigFileController
    {
        private const string UCD_CONFIG_FILENAME = "UcdConfig.dat";

        public bool LoadUcdConfig(ref UcdConfigModel ucdConfig)
        {
            if (ucdConfig == null)
            {
                ucdConfig = new UcdConfigModel();
            }

            if (!File.Exists(UCD_CONFIG_FILENAME))
            {
                return false;
            }

            bool isLoadingSuccessComplete = false;
            try
            {
                using (FileStream fileStream = File.Open(UCD_CONFIG_FILENAME, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader reader = new BinaryReader(fileStream, Encoding.UTF8, false))
                    {
                        ucdConfig.UcdPath = reader.ReadString();
                        ucdConfig.ProjectRootPath = reader.ReadString();
                        ucdConfig.EnvironmentType = (CCDEnvironmentTypes)reader.ReadInt32();
                        ucdConfig.BucketType = (CCDBucketTypes)reader.ReadInt32();
                        ucdConfig.IsCustomBadge = reader.ReadBoolean();
                        ucdConfig.CustomBadgeName = reader.ReadString();
                        ucdConfig.CLIType = (CommandLineInterfaceTypes)reader.ReadInt32();
                        isLoadingSuccessComplete = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: Handle exception here
            }
            return isLoadingSuccessComplete;
        }

        public UcdConfigModel LoadUcdConfig()
        {
            UcdConfigModel ucdConfig = new UcdConfigModel();
            LoadUcdConfig(ref ucdConfig);
            return ucdConfig;
        }

        public bool SaveUcdConfig(UcdConfigModel ucdConfig)
        {
            if (ucdConfig == null)
            {
                return false;
            }

            bool isSavingSuccess = false;
            try
            {
                using (FileStream stream = File.Open(UCD_CONFIG_FILENAME, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    using (BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8, false))
                    {
                        writer.Write(ucdConfig.UcdPath);
                        writer.Write(ucdConfig.ProjectRootPath);
                        writer.Write((int)ucdConfig.EnvironmentType);
                        writer.Write((int)ucdConfig.BucketType);
                        writer.Write(ucdConfig.IsCustomBadge);
                        writer.Write(ucdConfig.CustomBadgeName);
                        writer.Write((int)ucdConfig.CLIType);
                        isSavingSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: Handle exception here
            }
            return isSavingSuccess;
        }

    }
}
