﻿using AddressablesUCDController.CommandLineInterfaces;
using AddressablesUCDController.Enums;
using AddressablesUCDController.Models;

namespace AddressablesUCDController.Controllers
{
    public class UnityContentDeliveryController : IDisposable
    {

        #region Constants

        private const string SYNC_PATH_FORMAT = "{0}\\CCDBuildData\\{1}\\{2}\\{3}";    // {project_root_path}\CCDBuildData\{environment_id}\{bucket_id}\{badge_name}

        #endregion


        #region Public Events

        public event Action<string?>? OnOutputDataReceivedEvent = delegate { };
        public event Action<string?>? OnErrorDataReceivedEvent = delegate { };
        public event Action? OnLoggedInEvent = delegate { };
        public event Action? OnLoggedOutEvent = delegate { };
        public event Action<bool>? OnUCDReadyToSyncStateChangedEvent = delegate { };

        #endregion


        #region Private Properties

        private ICommandLineInterface? CommandLineInterface { get; set; }
        private IDCollectionModel IDCollectionModel { get; set; }
        private UCDSetupFlags UCDSetup { get; set; } = UCDSetupFlags.None;

        #endregion


        #region Public Properties

        public bool IsDisposed { get; private set; } = false;

        public bool IsCLIReady => HasUCDSetupFlag(UCDSetupFlags.IsCLIReady);
        public bool IsUCDPathReady => HasUCDSetupFlag(UCDSetupFlags.IsUCDPathReady);
        public bool IsLoggedIn => HasUCDSetupFlag(UCDSetupFlags.IsLoggedIn);
        public bool IsEnvironmentReady => HasUCDSetupFlag(UCDSetupFlags.IsEnvironmentReady);
        public bool IsBucketReady => HasUCDSetupFlag(UCDSetupFlags.IsBucketReady);
        public bool IsAllReady => UCDSetup == UCDSetupFlags.AllReady;
        
        public CommandLineInterfaceTypes CLIType { get; private set; } = CommandLineInterfaceTypes.None;
        public CCDEnvironmentTypes EnvironmentType { get; private set; } = CCDEnvironmentTypes.None;
        public CCDBucketTypes BucketType { get; private set; } = CCDBucketTypes.None;
        public string BadgeName { get; private set; } = "";

        #endregion


        #region Constructors

        public UnityContentDeliveryController(
            CommandLineInterfaceTypes commandLineInterfaceType,
            string ucdPath,
            Action<string?>? errorDataReceivedEventListeners = null
        )
        {
            if (errorDataReceivedEventListeners != null)
            {
                OnErrorDataReceivedEvent += errorDataReceivedEventListeners;
            }
            IDCollectionModel = new IDCollectionModel();
            InitializeCommandLineInterface(commandLineInterfaceType, ucdPath);
        }

        #endregion


        #region Finalizers

        public void Dispose()
        {
            if (IsDisposed) { return; }
            OnOutputDataReceivedEvent = null;
            OnErrorDataReceivedEvent = null;
            OnLoggedInEvent = null;
            OnLoggedOutEvent = null;
            OnUCDReadyToSyncStateChangedEvent = null;
            KillCommandLineInterface();
            ResetAllUCDSetupFlags();
            IsDisposed = true;
        }

        #endregion


        #region CLI Lifecycle

        private void InitializeCommandLineInterface(CommandLineInterfaceTypes commandLineInterfaceType, string ucdPath)
        {
            ChangeCommandLineInterface(commandLineInterfaceType, ucdPath);
        }

        public void ChangeCommandLineInterface(CommandLineInterfaceTypes commandLineInterfaceType, string ucdPath)
        {
            if (commandLineInterfaceType == CommandLineInterfaceTypes.Cmd)
            {
                InitializeCmdCommandLineInterface(ucdPath);
            }
            else if (commandLineInterfaceType == CommandLineInterfaceTypes.Terminal)
            {
                InitializeTerminalCommandLineInterface(ucdPath);
            }
            else
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                KillCommandLineInterface();
            }
        }

        private void InitializeCmdCommandLineInterface(string ucdPath)
        {
            KillCommandLineInterface();
            CommandLineInterface = new CmdCommandLineInterface(
                OnCommandLineInterfaceOutputDataReceivedEvent,
                OnCommandLineInterfaceErrorDataReceivedEvent
            );
            SetUCDSetupFlag(UCDSetupFlags.IsCLIReady);
            SetUcdPath(ucdPath);
            CLIType = CommandLineInterfaceTypes.Cmd;
        }

        private void InitializeTerminalCommandLineInterface(string ucdPath)
        {
            KillCommandLineInterface();
            CommandLineInterface = new TerminalCommandLineInterface(
                OnCommandLineInterfaceOutputDataReceivedEvent,
                OnCommandLineInterfaceErrorDataReceivedEvent
            );
            SetUCDSetupFlag(UCDSetupFlags.IsCLIReady);
            SetUcdPath(ucdPath);
            CLIType = CommandLineInterfaceTypes.Terminal;
        }

        private void KillCommandLineInterface()
        {
            if (IsLoggedIn)
            { 
                Logout();
            }
            ResetAllUCDSetupFlags();
            if (CommandLineInterface != null)
            {
                CommandLineInterface.Dispose();
                CommandLineInterface = null;
            }
        }

        #endregion


        #region UCD Commands

        public bool SetUcdPath(string ucdPath)
        {
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (IsLoggedIn)
            {
                Logout();
            }
            bool changeDirectoryResult = CommandLineInterface.ChangeDirectory(ucdPath);
            string combinedUcdFilePath = Path.Combine(ucdPath, "ucd.exe");
            if (!File.Exists(combinedUcdFilePath))
            {
                SendOutErrorMessage("Warning: There is no \"ucd.exe\" file found at destination.");
                return false;
            }

            if (changeDirectoryResult)
            {
                SetUCDSetupFlag(UCDSetupFlags.IsUCDPathReady);
            }
            return changeDirectoryResult;
        }

        public bool Login()
        {
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsUCDPathReady)
            {
                SendOutErrorMessage("Warning: UCD path is not ready yet.");
                return false;
            }
            if (!SendCommand(string.Format("ucd auth login {0}", IDCollectionModel.APIKey)))
            {
                return false;
            }
            SetUCDSetupFlag(UCDSetupFlags.IsLoggedIn);
            OnLoggedInEvent?.Invoke();
            return true;
        }

        public bool Logout()
        {
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsLoggedIn) { return false; }
            SendCommand("ucd auth logout");

            // After logout, we reset environment and bucket is ready bits.
            ResetUCDSetupFlag(UCDSetupFlags.IsLoggedIn | UCDSetupFlags.IsEnvironmentReady | UCDSetupFlags.IsBucketReady);
            OnLoggedOutEvent?.Invoke();
            return true;
        }

        public bool SetEnvironment(CCDEnvironmentTypes environmentType)
        {
            ResetUCDSetupFlag(UCDSetupFlags.IsEnvironmentReady);
            EnvironmentType = environmentType;
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsUCDPathReady)
            {
                SendOutErrorMessage("Warning: UCD path is not ready yet.");
                return false;
            }
            if (!IsLoggedIn)
            {
                SendOutErrorMessage("Warning: You need to login to set environment.");
                return false;
            }
            if (environmentType == CCDEnvironmentTypes.None)
            {
                SendOutErrorMessage("Warning: You must select an environment. Unable to configure UCD environment.");
                return false;
            }
            
            bool setEnvironmentResult = SendCommand(string.Format(
                "ucd config set environment {0} --project={1}",
                IDCollectionModel.GetEnvironmentID(environmentType),
                IDCollectionModel.ProjectID
            ));
            if (!setEnvironmentResult) { return false; }

            SetUCDSetupFlag(UCDSetupFlags.IsEnvironmentReady);
            return true;
        }

        public bool SetBucket(CCDBucketTypes bucketType)
        {
            ResetUCDSetupFlag(UCDSetupFlags.IsBucketReady);
            BucketType = bucketType;
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsUCDPathReady)
            {
                SendOutErrorMessage("Warning: UCD path is not ready yet.");
                return false;
            }
            if (!IsLoggedIn)
            {
                SendOutErrorMessage("Warning: You need to login to set bucket.");
                return false;
            }
            if (bucketType == CCDBucketTypes.None)
            {
                SendOutErrorMessage("Warning: You must select a bucket. Unable to configure UCD bucket.");
                BucketType = CCDBucketTypes.None;
                return false;
            }
            if (EnvironmentType == CCDEnvironmentTypes.None)
            {
                SendOutErrorMessage("Warning: UCD environment must be set before setting bucket.");
                return false;
            }
            if (!IsEnvironmentReady)
            {
                SetEnvironment(EnvironmentType);
            }
            if (!IsEnvironmentReady) { return false; }  // Environment is still not ready/set.

            string bucketID = IDCollectionModel.GetBucketID(EnvironmentType, bucketType);
            if (string.IsNullOrEmpty(bucketID))
            {
                SendOutErrorMessage(string.Format(
                    "Warning: There is no {0} bucket exists for the {1} environment.",
                    bucketType.ToString(),
                    EnvironmentType.ToString()
                ));
                return false;
            }
            bool setBucketResult = SendCommand(string.Format(
                "ucd config set bucket {0}",
                IDCollectionModel.GetBucketID(EnvironmentType, bucketType)
            ));
            if (!setBucketResult)
            {
                return false;
            }

            SetUCDSetupFlag(UCDSetupFlags.IsBucketReady);
            return true;
        }

        public bool SetBadgeName(string badgeName)
        {
            if (string.IsNullOrEmpty(badgeName))
            {
                SendOutErrorMessage("Warning: Empty or null Badge name is not allowed.");
                return false;
            }
            BadgeName = badgeName;
            return true;
        }

        public bool SyncCCDContentBuild(string projectRootPath)
        {
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsUCDPathReady)
            {
                SendOutErrorMessage("Warning: UCD path is not ready yet.");
                return false;
            }
            if (!IsLoggedIn)
            {
                SendOutErrorMessage("Warning: You need to login to set bucket.");
                return false;
            }

            if (string.IsNullOrEmpty(projectRootPath))
            {
                SendOutErrorMessage("Warning: Empty project root path is not allowed.");
                return false;
            }
            if (!Directory.Exists(projectRootPath))
            {
                SendOutErrorMessage(string.Format("Warning: There is no project root path like \"@{0}\"", projectRootPath));
                return false;
            }
            if (string.IsNullOrEmpty(BadgeName))
            {
                SendOutErrorMessage("Warning: Empty or null badge name.");
                return false;
            }
            string syncPath = string.Format(
                SYNC_PATH_FORMAT,
                projectRootPath,
                IDCollectionModel.GetEnvironmentID(EnvironmentType),
                IDCollectionModel.GetBucketID(EnvironmentType, BucketType),
                BadgeName
            );
            if (!Directory.Exists(syncPath))
            {
                SendOutErrorMessage(string.Format("Warning: There is no content build found at \"@{0}\"", syncPath));
                return false;
            }

            return CommandLineInterface.SendCommand(string.Format("ucd entries sync {0}", syncPath));
        }

        private bool SendCommand(string command)
        {
            if (!IsCLIReady)
            {
                SendOutErrorMessage("Warning: You need to select a CommandLineInterface (CLI).");
                return false;
            }
            if (!IsUCDPathReady)
            {
                SendOutErrorMessage("Warning: UCD path is not ready yet.");
                return false;
            }

            if (!HasUCDSetupFlag(UCDSetupFlags.IsCLIReady | UCDSetupFlags.IsUCDPathReady))
            {
                SendOutErrorMessage("Warning: Set a valid CLI type and UCD Path befure sending commands.");
                return false;
            }
            return CommandLineInterface.SendCommand(command);
        }

        #endregion


        #region UCD Setup Flag Utils

        public void ResetAllUCDSetupFlags()
        {
            UCDSetup = UCDSetupFlags.None;
            OnUCDReadyToSyncStateChangedEvent?.Invoke(false);
        }

        public void SetUCDSetupFlag(UCDSetupFlags setFlags)
        {
            UCDSetupFlags prevUCDSetup = UCDSetup;
            UCDSetup |= setFlags;
            if (UCDSetup != prevUCDSetup && IsAllReady)
            {
                OnUCDReadyToSyncStateChangedEvent?.Invoke(true);
            }
        }

        public void ResetUCDSetupFlag(UCDSetupFlags resetFlags)
        {
            UCDSetupFlags prevUCDSetup = UCDSetup;
            UCDSetup &= ~resetFlags;
            if (UCDSetup != prevUCDSetup)
            {
                OnUCDReadyToSyncStateChangedEvent?.Invoke(false);
            }
        }

        private bool HasUCDSetupFlag(UCDSetupFlags targetSetupFlag)
        {
            return (UCDSetup & targetSetupFlag) != 0;
        }

        #endregion


        #region Utils

        private void SendOutErrorMessage(string message)
        {
            OnErrorDataReceivedEvent?.Invoke(message);
        }

        #endregion


        #region CommandLineInterface (CLI) Events

        //NOTE: We can not use event properties here due to killing their references whenever CLIType changes.

        private void OnCommandLineInterfaceOutputDataReceivedEvent(string? data)
        {
            OnOutputDataReceivedEvent?.Invoke(data);
        }
        private void OnCommandLineInterfaceErrorDataReceivedEvent(string? data)
        {
            SendOutErrorMessage(data);
        }

        #endregion

    }
}
