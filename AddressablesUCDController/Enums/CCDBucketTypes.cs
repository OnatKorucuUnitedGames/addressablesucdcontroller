﻿namespace AddressablesUCDController.Enums
{
    public enum CCDBucketTypes
    {
        None = 0,
        Android = 1,
        IOS = 2
    }
}
