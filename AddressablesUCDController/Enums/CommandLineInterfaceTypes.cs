﻿namespace AddressablesUCDController.Enums
{
    public enum CommandLineInterfaceTypes
    {
        None = 0,

        /// <summary>
        ///     CLI on Windows operating system
        /// </summary>
        Cmd = 1,

        /// <summary>
        ///     CLI on Mac operating system
        /// </summary>
        Terminal = 2,
    }
}
