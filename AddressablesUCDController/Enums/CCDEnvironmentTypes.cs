﻿namespace AddressablesUCDController.Enums
{
    public enum CCDEnvironmentTypes
    {
        None = 0,

        Production = 1,
        OpenTesting = 2,
        ClosedTesting = 3,
        InternalTesting = 4,

        DevelopmentTest = 5,
        ArtTest = 6,
        NarratorTest = 7,
        
        DevED = 8,
        DevHakan = 9,
        DevKubilay = 10,
        DevSercan = 11,
        
        TechArtDjordje = 12,
    }
}
