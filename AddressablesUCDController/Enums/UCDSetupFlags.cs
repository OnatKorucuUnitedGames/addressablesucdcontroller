﻿namespace AddressablesUCDController.Enums
{
    [Flags]
    public  enum UCDSetupFlags
    {
        None = 0,

        IsCLIReady = 1 << 0,
        IsUCDPathReady = 1 << 1,
        IsLoggedIn = 1 << 2,
        IsEnvironmentReady = 1 << 3,
        IsBucketReady = 1 << 4,

        AllReady = ~(~0 << 5)
    }
}
