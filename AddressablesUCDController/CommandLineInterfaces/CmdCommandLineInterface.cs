﻿namespace AddressablesUCDController.CommandLineInterfaces
{
    public class CmdCommandLineInterface : BaseCommandLineInterface
    {
        protected override string CommandLineInterfaceFileName
        {
            get { return "cmd.exe"; }
        }
        protected override string DirectoryPathValidationRegex
        {
            get { return "(^([a-z]|[A-Z]):(?=\\\\(?![\\0-\\37<>:\"/\\\\|?*])|\\/(?![\\0-\\37<>:\"/\\\\|?*])|$)|^\\\\(?=[\\\\\\/][^\\0-\\37<>:\"/\\\\|?*]+)|^(?=(\\\\|\\/)$)|^\\.(?=(\\\\|\\/)$)|^\\.\\.(?=(\\\\|\\/)$)|^(?=(\\\\|\\/)[^\\0-\\37<>:\"/\\\\|?*]+)|^\\.(?=(\\\\|\\/)[^\\0-\\37<>:\"/\\\\|?*]+)|^\\.\\.(?=(\\\\|\\/)[^\\0-\\37<>:\"/\\\\|?*]+))((\\\\|\\/)[^\\0-\\37<>:\"/\\\\|?*]+|(\\\\|\\/)$)*()$"; }
        }

        public CmdCommandLineInterface()
            : base()
        { }

        public CmdCommandLineInterface(Action<string?>? outputDataReceivedEventListeners, Action<string?>? errorDataReceivedEventListeners)
            : base(outputDataReceivedEventListeners, errorDataReceivedEventListeners)
        { }

        public override bool ChangeDirectory(string directory)
        {
            if (!IsReady) { return false; }
            if (string.IsNullOrEmpty(directory)) { return false; }
            if (!Directory.Exists(directory)) { return false; }

            string driveName = directory.Substring(0, 1);
            SendCommand(string.Format("{0}:", driveName));      // Change drive
            SendCommand(string.Format("cd {0}", directory));    // Change directory in the new drive

            return true;
        }
    }
}
