﻿namespace AddressablesUCDController.CommandLineInterfaces
{
    public interface ICommandLineInterface : IDisposable
    {
        event Action<string?> OnOutputDataReceivedEvent;
        event Action<string?> OnErrorDataReceivedEvent;

        bool IsReady { get; }
        bool IsDisposed { get; }

        bool SendCommand(string command);
        bool ChangeDirectory(string directory);
    }
}
