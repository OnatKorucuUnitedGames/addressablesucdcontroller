﻿namespace AddressablesUCDController.CommandLineInterfaces
{
    public class TerminalCommandLineInterface : BaseCommandLineInterface
    {
        protected override string CommandLineInterfaceFileName
        {
            get { return @"/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal"; }
        }
        protected override string DirectoryPathValidationRegex
        {
            get { return "^\\/$|(^(?=\\/)|^\\.|^\\.\\.)(\\/(?=[^/\\0])[^/\\0]+)*\\/?$"; }
        }

        public TerminalCommandLineInterface()
            : base()
        { }

        public TerminalCommandLineInterface(Action<string?>? outputDataReceivedEventListeners, Action<string?>? errorDataReceivedEventListeners)
            : base(outputDataReceivedEventListeners, errorDataReceivedEventListeners)
        { }

        public override bool ChangeDirectory(string directory)
        {
            if (!IsReady) { return false; }
            if (string.IsNullOrEmpty(directory)) { return false; }
            if (!Directory.Exists(directory)) { return false; }

            SendCommand(string.Format("PATH=\"$PATH:{0}\"", directory));
            return true;
        }
    }
}
