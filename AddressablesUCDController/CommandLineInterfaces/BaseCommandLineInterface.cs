﻿using System.Diagnostics;
using Debug = System.Diagnostics.Debug;

namespace AddressablesUCDController.CommandLineInterfaces
{
    public abstract class BaseCommandLineInterface : ICommandLineInterface
    {
        public event Action<string?>? OnOutputDataReceivedEvent = delegate { };
        public event Action<string?>? OnErrorDataReceivedEvent = delegate { };

        public bool IsReady
        {
            get { return CLIProcess != null && CLIProcessID != 0 && !CLIProcess.HasExited; }
        }
        public bool IsDisposed { get; private set; } = false;

        private Process? CLIProcess { get; set; } = null;
        private int CLIProcessID { get; set; } = 0;

        protected abstract string CommandLineInterfaceFileName { get; }
        protected abstract string DirectoryPathValidationRegex { get; }

        public BaseCommandLineInterface()
            : this(null, null)
        { }

        public BaseCommandLineInterface(Action<string?>? outputDataReceivedEventListeners, Action<string?>? errorDataReceivedEventListeners)
        {
            if (outputDataReceivedEventListeners != null)
            {
                OnOutputDataReceivedEvent += outputDataReceivedEventListeners;
            }
            if (errorDataReceivedEventListeners != null)
            {
                OnErrorDataReceivedEvent += errorDataReceivedEventListeners;
            }
            InitializeCLIProcess();
        }

        public void Dispose()
        {
            if (IsDisposed) { return; }
            OnBeforeDisposing();
            CloseCLIProcess();
            OnOutputDataReceivedEvent = null;
            OnErrorDataReceivedEvent = null;
            OnAfterDisposed();
            IsDisposed = true;
        }

        public virtual void OnBeforeDisposing() { }
        public virtual void OnAfterDisposed() { }


        public bool SendCommand(string command)
        {
            if (IsDisposed) { return false; }
            if (!IsReady) { return false; }
            if (string.IsNullOrEmpty(command)) { return false; }

            CLIProcess.StandardInput.WriteLine(command);
            CLIProcess.StandardInput.Flush();
            return true;
        }

        public abstract bool ChangeDirectory(string directory);


        private void OnCLIProcessOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string? outputData = e.Data;
            OnOutputDataReceived(outputData);
            OnOutputDataReceivedEvent.Invoke(outputData);
        }

        private void OnCLIProcessErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            string? errorData = e.Data;
            OnErrorDataReceived(errorData);
            OnErrorDataReceivedEvent.Invoke(errorData);
        }

        protected virtual void OnOutputDataReceived(string? data) { }
        protected virtual void OnErrorDataReceived(string? data) { }


        private void InitializeCLIProcess()
        {
            if (IsDisposed) { return; }
            if (CLIProcess != null) { return; }

            CLIProcess = new Process();

            // Configure start info
            CLIProcess.StartInfo.FileName = CommandLineInterfaceFileName;
            CLIProcess.StartInfo.UseShellExecute = false;
            CLIProcess.StartInfo.RedirectStandardInput = true;
            CLIProcess.StartInfo.RedirectStandardOutput = true;
            CLIProcess.StartInfo.RedirectStandardError = true;
            CLIProcess.StartInfo.CreateNoWindow = true;

            // Register to data received events
            CLIProcess.OutputDataReceived += OnCLIProcessOutputDataReceived;
            CLIProcess.ErrorDataReceived += OnCLIProcessErrorDataReceived;

            // Start process and begin line readings
            try
            {
                CLIProcess.Start();
                CLIProcessID = CLIProcess.Id;
                CLIProcess.BeginOutputReadLine();
                CLIProcess.BeginErrorReadLine();
            }
            catch (Exception ex)
            {
                CLIProcessID = 0;
                string errorMsg = string.Format("Error: {0}", ex.Message.ToString());
                Debug.WriteLine(errorMsg);
                OnErrorDataReceived(errorMsg);
                OnErrorDataReceivedEvent(errorMsg);
            }
        }

        private void CloseCLIProcess()
        {
            if (CLIProcess == null) { return; }
            CLIProcess.Close();
            CLIProcessID = 0;
            CLIProcess = null;
        }

        private void KillCLIProcess()
        {
            if (CLIProcess == null) { return; }
            CLIProcess.Dispose();
            CLIProcessID = 0;
            CLIProcess = null;
        }

    }
}
