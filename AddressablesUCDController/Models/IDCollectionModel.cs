﻿using AddressablesUCDController.Enums;

namespace AddressablesUCDController.Models
{
    public class IDCollectionModel
    {
        public string ProjectID
        {
            get { return GetProjectID(); }
        }
        public string APIKey
        {
            get { return GetAPIKey(); }
        }


        private string GetProjectID()
        {
            return "87bf6ea2-977b-4f12-94bf-6cf6049f2cb0";
        }

        private string GetAPIKey()
        {
            return "bbc4b7261c702c470393f7b6a68c0df1";
        }

        public string GetEnvironmentID(CCDEnvironmentTypes environmentType)
        {
            Dictionary<CCDEnvironmentTypes, string> environmentIDs = new Dictionary<CCDEnvironmentTypes, string>()
            {
                { CCDEnvironmentTypes.None, "" },

                { CCDEnvironmentTypes.Production, "999f02a6-c23f-4f20-983d-c763febf1089" },
                { CCDEnvironmentTypes.OpenTesting, "2ac6ceac-17a7-4d28-8b42-dcac181c5024" },
                { CCDEnvironmentTypes.ClosedTesting, "19aad44b-f8a8-4cd4-ad9e-76515a8d4e58" },
                { CCDEnvironmentTypes.InternalTesting, "1019161c-33d7-4e3c-a0ac-2bfd21e22514" },

                { CCDEnvironmentTypes.DevelopmentTest, "c9ac4887-0391-4e03-91b2-f725617dbe27" },
                { CCDEnvironmentTypes.ArtTest, "6457f70f-24bd-4eda-8104-0b382822f4c2" },
                { CCDEnvironmentTypes.NarratorTest, "788eedca-c6ba-4b54-bea1-cb5f1dad4390" },

                { CCDEnvironmentTypes.DevED, "d3598efe-445b-4f2a-a276-26291b5de18c" },
                { CCDEnvironmentTypes.DevHakan, "4b2c34c7-60ac-43c6-a687-deb3eafb8fb5" },
                { CCDEnvironmentTypes.DevKubilay, "2f4e27f9-bbe0-4d47-b4b0-4353393c3a4c" },
                { CCDEnvironmentTypes.DevSercan, "066b0d23-3fe4-4905-95ba-61e8fe3359e9" },
                
                { CCDEnvironmentTypes.TechArtDjordje, "d3457088-3833-4490-a6b1-6d9ac282bdb5" },
            };
            return environmentIDs[environmentType];
        }

        public string GetBucketID(CCDEnvironmentTypes environmentType, CCDBucketTypes bucketType)
        {
            Dictionary<CCDEnvironmentTypes, Dictionary<CCDBucketTypes, string>> BucketIDs = new Dictionary<CCDEnvironmentTypes, Dictionary<CCDBucketTypes, string>>()
            {
                // None environment
                {
                    CCDEnvironmentTypes.None,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "" },
                        { CCDBucketTypes.IOS, "" },
                    }
                },
                //----------------------------------------------------------------------------------------------------------
                // Production environment
                {
                    CCDEnvironmentTypes.Production,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "e95999ba-828f-42c7-914e-83ed47177ef5" },
                        { CCDBucketTypes.IOS, "65451b8d-cc73-40cc-80a7-dade0787e722" },
                    }
                },

                // OpenTesting environment
                {
                    CCDEnvironmentTypes.OpenTesting,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "80173cf7-f5e4-42ae-b6f6-a756520b346b" },
                        { CCDBucketTypes.IOS, "13103822-bb41-445b-92be-ae6fc786d2a7" },
                    }
                },

                // ClosedTesting  environment
                {
                    CCDEnvironmentTypes.ClosedTesting,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "8e106cc0-8971-4e2f-87d1-72db6929c68c" },
                        { CCDBucketTypes.IOS, "6251ba45-62da-47ff-b242-bcbbc0a3c075" },
                    }
                },

                // InternalTesting environment
                {
                    CCDEnvironmentTypes.InternalTesting,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "637155de-e91d-4682-b27e-b39a9afae82e" },
                        { CCDBucketTypes.IOS, "2891f094-519d-4582-bf5b-213ec6cef976" },
                    }
                },
                //----------------------------------------------------------------------------------------------------------
                // DevelopmentTest environment
                {
                    CCDEnvironmentTypes.DevelopmentTest,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "d54206e0-220a-4e6e-890e-f613989d46f7" },
                        { CCDBucketTypes.IOS, "1b04d53e-c757-4979-b7f7-175e37485241" },
                    }
                },

                // ArtTest environment
                {
                    CCDEnvironmentTypes.ArtTest,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "1f59e07b-85f3-4c87-a55c-8f89c87079f3" },
                        { CCDBucketTypes.IOS, "fca97ca0-f9f4-4bf3-85b3-8eef7e0a7fff" },
                    }
                },

                // NarratorTest environment
                {
                    CCDEnvironmentTypes.NarratorTest,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "795986f2-d6e9-4be9-8e70-458dcae0d97f" },
                        { CCDBucketTypes.IOS, "63127082-f2d6-4ba7-909b-7463686f45ba" },
                    }
                },
                //----------------------------------------------------------------------------------------------------------
                // DevED environment
                {
                    CCDEnvironmentTypes.DevED,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "ac181fb5-1527-4d8d-8008-8d60e8e3ff87" },
                        { CCDBucketTypes.IOS, "f7f2f10f-2fda-4523-b10d-7760c7313e69" },
                    }
                },

                // DevHakan environment
                {
                    CCDEnvironmentTypes.DevHakan,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "1d02a8f8-270f-4d28-8f81-94f46c43f6bc" },
                        { CCDBucketTypes.IOS, "3c247c98-a06e-4a84-9a8e-9b9987860f30" },
                    }
                },

                // DevKubilay environment
                {
                    CCDEnvironmentTypes.DevKubilay,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "749d28da-8981-492f-bb29-cc1d6b094b45" },
                        { CCDBucketTypes.IOS, "547c56a8-b4ca-4713-bdc5-76cd2fe62c3a" },
                    }
                },

                // DevSercan environment
                {
                    CCDEnvironmentTypes.DevSercan,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "bdc42e95-f1c5-4019-9dc0-8b8f6d5345ab" },
                        { CCDBucketTypes.IOS, "ced7af96-1ee5-4edc-8f12-822e4a6dcf50" },
                    }
                },
                //----------------------------------------------------------------------------------------------------------
                // TechArtDjordje environment
                {
                    CCDEnvironmentTypes.TechArtDjordje,
                    new Dictionary<CCDBucketTypes, string>()
                    {
                        { CCDBucketTypes.None, "" },
                        { CCDBucketTypes.Android, "3585d8a4-2894-46aa-907a-46cd0faa40b4" },
                        { CCDBucketTypes.IOS, "5b92ee5e-80b8-4f10-a4e9-156537136ab1" },
                    }
                },
            };
            return BucketIDs[environmentType][bucketType];
        }


    }
}
