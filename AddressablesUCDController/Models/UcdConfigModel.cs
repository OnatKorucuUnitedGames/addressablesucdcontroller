﻿using AddressablesUCDController.Enums;

namespace AddressablesUCDController.Models
{
    public class UcdConfigModel
    {
        public string UcdPath { get; internal set; } = "";
        public string ProjectRootPath { get; internal set; } = "";
        public CCDEnvironmentTypes EnvironmentType { get; internal set; } = CCDEnvironmentTypes.None;
        public CCDBucketTypes BucketType { get; internal set; } = CCDBucketTypes.None;
        public bool IsCustomBadge { get; internal set; } = false;
        public string CustomBadgeName { get; internal set; } = "v_1_0_0";
        public CommandLineInterfaceTypes CLIType { get; internal set; } = CommandLineInterfaceTypes.None;
    }
}
