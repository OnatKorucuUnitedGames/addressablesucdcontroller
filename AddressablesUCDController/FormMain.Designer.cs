﻿namespace LoveEdenCCDControllerForm
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            LoginButton = new Button();
            UcdOutputGroupBox = new GroupBox();
            CLIOutputRichTextBox = new RichTextBox();
            FolderBrowserDialog = new FolderBrowserDialog();
            SelectProjectRootPathButton = new Button();
            ProjectRootFolderTextBox = new TextBox();
            TopTableLayoutPanel = new TableLayoutPanel();
            CLITypePickerComboBox = new ComboBox();
            CLITypePickerLabel = new Label();
            BadgeTableLayoutPanel = new TableLayoutPanel();
            UseDefaultBadgeRadioButton = new RadioButton();
            UseCustomBadgeRadioButton = new RadioButton();
            CustomBadgePanel = new Panel();
            CustomBadgeNameTextBox = new TextBox();
            CustomBadgeNameLabel = new Label();
            BadgePickerLabel = new Label();
            BucketPickerComboBox = new ComboBox();
            BucketPickerLabel = new Label();
            EnvironmentPickerComboBox = new ComboBox();
            EnvironmentPickerLabel = new Label();
            UcdPathTextBox = new TextBox();
            SelectUcdPathButton = new Button();
            ButtonPositionerTableLayoutPanel = new TableLayoutPanel();
            ClearMessagesButton = new Button();
            SyncButton = new Button();
            UcdControlButtonsTableLayoutPanel = new TableLayoutPanel();
            UcdOutputGroupBox.SuspendLayout();
            TopTableLayoutPanel.SuspendLayout();
            BadgeTableLayoutPanel.SuspendLayout();
            CustomBadgePanel.SuspendLayout();
            ButtonPositionerTableLayoutPanel.SuspendLayout();
            UcdControlButtonsTableLayoutPanel.SuspendLayout();
            SuspendLayout();
            // 
            // LoginButton
            // 
            LoginButton.Dock = DockStyle.Fill;
            LoginButton.Location = new Point(15, 10);
            LoginButton.Margin = new Padding(15, 10, 15, 10);
            LoginButton.Name = "LoginButton";
            LoginButton.Size = new Size(141, 25);
            LoginButton.TabIndex = 0;
            LoginButton.Text = "Login";
            LoginButton.UseVisualStyleBackColor = true;
            LoginButton.Click += LoginButton_Click;
            // 
            // UcdOutputGroupBox
            // 
            UcdOutputGroupBox.Controls.Add(CLIOutputRichTextBox);
            UcdOutputGroupBox.Dock = DockStyle.Fill;
            UcdOutputGroupBox.Location = new Point(0, 250);
            UcdOutputGroupBox.Name = "UcdOutputGroupBox";
            UcdOutputGroupBox.Size = new Size(784, 311);
            UcdOutputGroupBox.TabIndex = 1;
            UcdOutputGroupBox.TabStop = false;
            UcdOutputGroupBox.Text = "Ucd Output";
            // 
            // CLIOutputRichTextBox
            // 
            CLIOutputRichTextBox.BackColor = Color.Linen;
            CLIOutputRichTextBox.Dock = DockStyle.Fill;
            CLIOutputRichTextBox.ForeColor = Color.SaddleBrown;
            CLIOutputRichTextBox.Location = new Point(3, 19);
            CLIOutputRichTextBox.Name = "CLIOutputRichTextBox";
            CLIOutputRichTextBox.ReadOnly = true;
            CLIOutputRichTextBox.Size = new Size(778, 289);
            CLIOutputRichTextBox.TabIndex = 0;
            CLIOutputRichTextBox.Text = "";
            // 
            // SelectProjectRootPathButton
            // 
            SelectProjectRootPathButton.Dock = DockStyle.Fill;
            SelectProjectRootPathButton.Location = new Point(9, 6);
            SelectProjectRootPathButton.Name = "SelectProjectRootPathButton";
            SelectProjectRootPathButton.Size = new Size(144, 23);
            SelectProjectRootPathButton.TabIndex = 2;
            SelectProjectRootPathButton.Text = "Project Root Path:";
            SelectProjectRootPathButton.TextAlign = ContentAlignment.MiddleRight;
            SelectProjectRootPathButton.UseVisualStyleBackColor = true;
            SelectProjectRootPathButton.Click += SelectProjectRootPathButton_Click;
            // 
            // ProjectRootFolderTextBox
            // 
            ProjectRootFolderTextBox.BackColor = SystemColors.Control;
            ProjectRootFolderTextBox.BorderStyle = BorderStyle.None;
            ProjectRootFolderTextBox.Dock = DockStyle.Fill;
            ProjectRootFolderTextBox.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            ProjectRootFolderTextBox.ForeColor = SystemColors.GrayText;
            ProjectRootFolderTextBox.Location = new Point(162, 6);
            ProjectRootFolderTextBox.Multiline = true;
            ProjectRootFolderTextBox.Name = "ProjectRootFolderTextBox";
            ProjectRootFolderTextBox.ReadOnly = true;
            ProjectRootFolderTextBox.Size = new Size(613, 23);
            ProjectRootFolderTextBox.TabIndex = 3;
            ProjectRootFolderTextBox.Text = "D:\\abc";
            // 
            // TopTableLayoutPanel
            // 
            TopTableLayoutPanel.AutoSize = true;
            TopTableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.OutsetPartial;
            TopTableLayoutPanel.ColumnCount = 2;
            TopTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            TopTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            TopTableLayoutPanel.Controls.Add(CLITypePickerComboBox, 1, 5);
            TopTableLayoutPanel.Controls.Add(CLITypePickerLabel, 0, 5);
            TopTableLayoutPanel.Controls.Add(BadgeTableLayoutPanel, 1, 4);
            TopTableLayoutPanel.Controls.Add(BadgePickerLabel, 0, 4);
            TopTableLayoutPanel.Controls.Add(BucketPickerComboBox, 1, 3);
            TopTableLayoutPanel.Controls.Add(BucketPickerLabel, 0, 3);
            TopTableLayoutPanel.Controls.Add(EnvironmentPickerComboBox, 1, 2);
            TopTableLayoutPanel.Controls.Add(EnvironmentPickerLabel, 0, 2);
            TopTableLayoutPanel.Controls.Add(UcdPathTextBox, 1, 1);
            TopTableLayoutPanel.Controls.Add(SelectUcdPathButton, 0, 1);
            TopTableLayoutPanel.Controls.Add(ProjectRootFolderTextBox, 1, 0);
            TopTableLayoutPanel.Controls.Add(SelectProjectRootPathButton, 0, 0);
            TopTableLayoutPanel.Dock = DockStyle.Top;
            TopTableLayoutPanel.Location = new Point(0, 0);
            TopTableLayoutPanel.Name = "TopTableLayoutPanel";
            TopTableLayoutPanel.Padding = new Padding(3, 0, 3, 0);
            TopTableLayoutPanel.RowCount = 6;
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 29F));
            TopTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            TopTableLayoutPanel.Size = new Size(784, 195);
            TopTableLayoutPanel.TabIndex = 8;
            // 
            // CLITypePickerComboBox
            // 
            CLITypePickerComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            CLITypePickerComboBox.FormattingEnabled = true;
            CLITypePickerComboBox.Location = new Point(162, 166);
            CLITypePickerComboBox.Name = "CLITypePickerComboBox";
            CLITypePickerComboBox.Size = new Size(191, 23);
            CLITypePickerComboBox.TabIndex = 2;
            // 
            // CLITypePickerLabel
            // 
            CLITypePickerLabel.Dock = DockStyle.Fill;
            CLITypePickerLabel.Location = new Point(9, 163);
            CLITypePickerLabel.Name = "CLITypePickerLabel";
            CLITypePickerLabel.Size = new Size(144, 29);
            CLITypePickerLabel.TabIndex = 3;
            CLITypePickerLabel.Text = "CLI Type:";
            CLITypePickerLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // BadgeTableLayoutPanel
            // 
            BadgeTableLayoutPanel.ColumnCount = 3;
            BadgeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
            BadgeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 90F));
            BadgeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            BadgeTableLayoutPanel.Controls.Add(UseDefaultBadgeRadioButton, 0, 0);
            BadgeTableLayoutPanel.Controls.Add(UseCustomBadgeRadioButton, 1, 0);
            BadgeTableLayoutPanel.Controls.Add(CustomBadgePanel, 2, 0);
            BadgeTableLayoutPanel.Dock = DockStyle.Fill;
            BadgeTableLayoutPanel.Location = new Point(162, 134);
            BadgeTableLayoutPanel.Name = "BadgeTableLayoutPanel";
            BadgeTableLayoutPanel.RowCount = 1;
            BadgeTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            BadgeTableLayoutPanel.Size = new Size(613, 23);
            BadgeTableLayoutPanel.TabIndex = 4;
            // 
            // UseDefaultBadgeRadioButton
            // 
            UseDefaultBadgeRadioButton.AutoSize = true;
            UseDefaultBadgeRadioButton.Dock = DockStyle.Fill;
            UseDefaultBadgeRadioButton.ImageAlign = ContentAlignment.MiddleLeft;
            UseDefaultBadgeRadioButton.Location = new Point(3, 3);
            UseDefaultBadgeRadioButton.Name = "UseDefaultBadgeRadioButton";
            UseDefaultBadgeRadioButton.Padding = new Padding(5, 0, 5, 0);
            UseDefaultBadgeRadioButton.Size = new Size(114, 17);
            UseDefaultBadgeRadioButton.TabIndex = 4;
            UseDefaultBadgeRadioButton.Text = "Default (latest)";
            UseDefaultBadgeRadioButton.UseVisualStyleBackColor = true;
            UseDefaultBadgeRadioButton.CheckedChanged += UseDefaultBadgeRadioButton_CheckedChanged;
            // 
            // UseCustomBadgeRadioButton
            // 
            UseCustomBadgeRadioButton.AutoSize = true;
            UseCustomBadgeRadioButton.Dock = DockStyle.Fill;
            UseCustomBadgeRadioButton.ImageAlign = ContentAlignment.MiddleLeft;
            UseCustomBadgeRadioButton.Location = new Point(123, 3);
            UseCustomBadgeRadioButton.Name = "UseCustomBadgeRadioButton";
            UseCustomBadgeRadioButton.Padding = new Padding(5, 0, 5, 0);
            UseCustomBadgeRadioButton.Size = new Size(84, 17);
            UseCustomBadgeRadioButton.TabIndex = 5;
            UseCustomBadgeRadioButton.Text = "Custom";
            UseCustomBadgeRadioButton.UseVisualStyleBackColor = true;
            UseCustomBadgeRadioButton.CheckedChanged += UseCustomBadgeRadioButton_CheckedChanged;
            // 
            // CustomBadgePanel
            // 
            CustomBadgePanel.Controls.Add(CustomBadgeNameTextBox);
            CustomBadgePanel.Controls.Add(CustomBadgeNameLabel);
            CustomBadgePanel.Dock = DockStyle.Fill;
            CustomBadgePanel.Location = new Point(213, 1);
            CustomBadgePanel.Margin = new Padding(3, 1, 3, 2);
            CustomBadgePanel.Name = "CustomBadgePanel";
            CustomBadgePanel.Size = new Size(397, 20);
            CustomBadgePanel.TabIndex = 6;
            // 
            // CustomBadgeNameTextBox
            // 
            CustomBadgeNameTextBox.BackColor = Color.Linen;
            CustomBadgeNameTextBox.Dock = DockStyle.Left;
            CustomBadgeNameTextBox.ForeColor = Color.SaddleBrown;
            CustomBadgeNameTextBox.Location = new Point(91, 0);
            CustomBadgeNameTextBox.MaxLength = 255;
            CustomBadgeNameTextBox.Name = "CustomBadgeNameTextBox";
            CustomBadgeNameTextBox.Size = new Size(209, 23);
            CustomBadgeNameTextBox.TabIndex = 7;
            CustomBadgeNameTextBox.Text = "latest";
            CustomBadgeNameTextBox.Leave += OnCustomBadgeNameTextBoxLeave;
            // 
            // CustomBadgeNameLabel
            // 
            CustomBadgeNameLabel.Dock = DockStyle.Left;
            CustomBadgeNameLabel.Location = new Point(0, 0);
            CustomBadgeNameLabel.Name = "CustomBadgeNameLabel";
            CustomBadgeNameLabel.Size = new Size(91, 20);
            CustomBadgeNameLabel.TabIndex = 6;
            CustomBadgeNameLabel.Text = "Badge Name:";
            CustomBadgeNameLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // BadgePickerLabel
            // 
            BadgePickerLabel.Dock = DockStyle.Fill;
            BadgePickerLabel.Location = new Point(9, 131);
            BadgePickerLabel.Name = "BadgePickerLabel";
            BadgePickerLabel.Size = new Size(144, 29);
            BadgePickerLabel.TabIndex = 3;
            BadgePickerLabel.Text = "Badge:";
            BadgePickerLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // BucketPickerComboBox
            // 
            BucketPickerComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            BucketPickerComboBox.FormattingEnabled = true;
            BucketPickerComboBox.Location = new Point(162, 102);
            BucketPickerComboBox.Name = "BucketPickerComboBox";
            BucketPickerComboBox.Size = new Size(191, 23);
            BucketPickerComboBox.TabIndex = 2;
            // 
            // BucketPickerLabel
            // 
            BucketPickerLabel.Dock = DockStyle.Fill;
            BucketPickerLabel.Location = new Point(9, 99);
            BucketPickerLabel.Name = "BucketPickerLabel";
            BucketPickerLabel.Size = new Size(144, 29);
            BucketPickerLabel.TabIndex = 3;
            BucketPickerLabel.Text = "Bucket:";
            BucketPickerLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // EnvironmentPickerComboBox
            // 
            EnvironmentPickerComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            EnvironmentPickerComboBox.FormattingEnabled = true;
            EnvironmentPickerComboBox.Location = new Point(162, 70);
            EnvironmentPickerComboBox.Name = "EnvironmentPickerComboBox";
            EnvironmentPickerComboBox.Size = new Size(191, 23);
            EnvironmentPickerComboBox.TabIndex = 2;
            // 
            // EnvironmentPickerLabel
            // 
            EnvironmentPickerLabel.Dock = DockStyle.Fill;
            EnvironmentPickerLabel.Location = new Point(9, 67);
            EnvironmentPickerLabel.Name = "EnvironmentPickerLabel";
            EnvironmentPickerLabel.Size = new Size(144, 29);
            EnvironmentPickerLabel.TabIndex = 3;
            EnvironmentPickerLabel.Text = "Environment:";
            EnvironmentPickerLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // UcdPathTextBox
            // 
            UcdPathTextBox.BackColor = SystemColors.Control;
            UcdPathTextBox.BorderStyle = BorderStyle.None;
            UcdPathTextBox.Dock = DockStyle.Fill;
            UcdPathTextBox.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            UcdPathTextBox.ForeColor = SystemColors.GrayText;
            UcdPathTextBox.Location = new Point(162, 38);
            UcdPathTextBox.Multiline = true;
            UcdPathTextBox.Name = "UcdPathTextBox";
            UcdPathTextBox.ReadOnly = true;
            UcdPathTextBox.Size = new Size(613, 23);
            UcdPathTextBox.TabIndex = 3;
            UcdPathTextBox.Text = "D:\\abc";
            // 
            // SelectUcdPathButton
            // 
            SelectUcdPathButton.Dock = DockStyle.Fill;
            SelectUcdPathButton.Location = new Point(9, 38);
            SelectUcdPathButton.Name = "SelectUcdPathButton";
            SelectUcdPathButton.Size = new Size(144, 23);
            SelectUcdPathButton.TabIndex = 2;
            SelectUcdPathButton.Text = "Ucd Path:";
            SelectUcdPathButton.TextAlign = ContentAlignment.MiddleRight;
            SelectUcdPathButton.UseVisualStyleBackColor = true;
            SelectUcdPathButton.Click += SelectUcdPathButton_Click;
            // 
            // ButtonPositionerTableLayoutPanel
            // 
            ButtonPositionerTableLayoutPanel.ColumnCount = 3;
            ButtonPositionerTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3333321F));
            ButtonPositionerTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3333321F));
            ButtonPositionerTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3333321F));
            ButtonPositionerTableLayoutPanel.Controls.Add(ClearMessagesButton, 2, 0);
            ButtonPositionerTableLayoutPanel.Controls.Add(SyncButton, 1, 0);
            ButtonPositionerTableLayoutPanel.Controls.Add(LoginButton, 0, 0);
            ButtonPositionerTableLayoutPanel.Dock = DockStyle.Left;
            ButtonPositionerTableLayoutPanel.Location = new Point(150, 3);
            ButtonPositionerTableLayoutPanel.Margin = new Padding(0);
            ButtonPositionerTableLayoutPanel.Name = "ButtonPositionerTableLayoutPanel";
            ButtonPositionerTableLayoutPanel.RowCount = 1;
            ButtonPositionerTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            ButtonPositionerTableLayoutPanel.Size = new Size(515, 45);
            ButtonPositionerTableLayoutPanel.TabIndex = 0;
            // 
            // ClearMessagesButton
            // 
            ClearMessagesButton.Dock = DockStyle.Fill;
            ClearMessagesButton.Location = new Point(357, 10);
            ClearMessagesButton.Margin = new Padding(15, 10, 15, 10);
            ClearMessagesButton.Name = "ClearMessagesButton";
            ClearMessagesButton.Size = new Size(143, 25);
            ClearMessagesButton.TabIndex = 2;
            ClearMessagesButton.Text = "Clear Messages";
            ClearMessagesButton.UseVisualStyleBackColor = true;
            ClearMessagesButton.Click += ClearMessagesButton_Click;
            // 
            // SyncButton
            // 
            SyncButton.Dock = DockStyle.Fill;
            SyncButton.Location = new Point(186, 10);
            SyncButton.Margin = new Padding(15, 10, 15, 10);
            SyncButton.Name = "SyncButton";
            SyncButton.Size = new Size(141, 25);
            SyncButton.TabIndex = 1;
            SyncButton.Text = "Sync";
            SyncButton.UseVisualStyleBackColor = true;
            SyncButton.Click += SyncButton_Click;
            // 
            // UcdControlButtonsTableLayoutPanel
            // 
            UcdControlButtonsTableLayoutPanel.ColumnCount = 2;
            UcdControlButtonsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            UcdControlButtonsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            UcdControlButtonsTableLayoutPanel.Controls.Add(ButtonPositionerTableLayoutPanel, 1, 1);
            UcdControlButtonsTableLayoutPanel.Dock = DockStyle.Top;
            UcdControlButtonsTableLayoutPanel.Location = new Point(0, 195);
            UcdControlButtonsTableLayoutPanel.Name = "UcdControlButtonsTableLayoutPanel";
            UcdControlButtonsTableLayoutPanel.RowCount = 3;
            UcdControlButtonsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 35F));
            UcdControlButtonsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 45F));
            UcdControlButtonsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 65F));
            UcdControlButtonsTableLayoutPanel.Size = new Size(784, 55);
            UcdControlButtonsTableLayoutPanel.TabIndex = 0;
            // 
            // FormMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(784, 561);
            Controls.Add(UcdOutputGroupBox);
            Controls.Add(UcdControlButtonsTableLayoutPanel);
            Controls.Add(TopTableLayoutPanel);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MinimumSize = new Size(700, 500);
            Name = "FormMain";
            Text = "Love Eden - Ucd Control";
            UcdOutputGroupBox.ResumeLayout(false);
            TopTableLayoutPanel.ResumeLayout(false);
            TopTableLayoutPanel.PerformLayout();
            BadgeTableLayoutPanel.ResumeLayout(false);
            BadgeTableLayoutPanel.PerformLayout();
            CustomBadgePanel.ResumeLayout(false);
            CustomBadgePanel.PerformLayout();
            ButtonPositionerTableLayoutPanel.ResumeLayout(false);
            UcdControlButtonsTableLayoutPanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button LoginButton;
        private GroupBox UcdOutputGroupBox;
        private RichTextBox CLIOutputRichTextBox;
        private FolderBrowserDialog FolderBrowserDialog;
        private Button SelectProjectRootPathButton;
        private TextBox ProjectRootFolderTextBox;
        private TextBox UcdPathTextBox;
        private Button SelectUcdPathButton;
        private GroupBox ControlButtonsGroupBox;
        private Label EnvironmentPickerLabel;
        private ComboBox EnvironmentPickerComboBox;
        private Button SyncButton;
        private ComboBox CLITypePickerComboBox;
        private Label CLITypePickerLabel;
        private ComboBox BucketPickerComboBox;
        private Label BucketPickerLabel;
        private Button ClearMessagesButton;
        private RadioButton UseCustomBadgeRadioButton;
        private RadioButton UseDefaultBadgeRadioButton;
        private Label BadgePickerLabel;
        private TextBox CustomBadgeNameTextBox;
        private Label CustomBadgeNameLabel;
        private TableLayoutPanel TopTableLayoutPanel;
        private TableLayoutPanel BadgeTableLayoutPanel;
        private Panel CustomBadgePanel;
        private TableLayoutPanel ButtonPositionerTableLayoutPanel;
        private TableLayoutPanel UcdControlButtonsTableLayoutPanel;
    }
}