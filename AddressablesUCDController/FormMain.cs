using AddressablesUCDController.Controllers;
using AddressablesUCDController.Enums;
using AddressablesUCDController.Models;

namespace LoveEdenCCDControllerForm
{
    public partial class FormMain : Form
    {

        #region Constants

        private const string DEFAULT_BADGE_NAME = "latest";
        private static readonly Color ErrorMessageColor = Color.Red;
        private const string SEPARATOR_LINE_TEXT = "---------------------------------------";

        #endregion


        #region Properties

        private UcdConfigFileController? UcdConfigFileController { get; set; } = null;
        private UcdConfigModel? UcdConfigModel { get; set; } = null;
        private UnityContentDeliveryController? UcdController { get; set; } = null;

        #endregion


        #region Initializers

        public FormMain()
        {
            InitializeComponent();
        }

        #endregion

        #region Form Events

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            UcdConfigFileController = new UcdConfigFileController();
            UcdConfigModel = UcdConfigFileController.LoadUcdConfig();

            ProjectRootFolderTextBox.Text = UcdConfigModel.ProjectRootPath;
            UcdPathTextBox.Text = UcdConfigModel.UcdPath;

            InitializeEnvironmentPickerComboBox();
            InitializeBucketPickerComboBox();
            InitializeCLITypePickerComboBox();

            SyncButton.Enabled = false;
            //EnvironmentPickerComboBox.Enabled = false;
            //BucketPickerComboBox.Enabled = false;

            UcdController = new UnityContentDeliveryController(UcdConfigModel.CLIType, UcdConfigModel.UcdPath, OnUcdControllerErrorDataReceived);
            UcdController.OnOutputDataReceivedEvent += OnUcdControllerOutputDataReceived;
            UcdController.OnUCDReadyToSyncStateChangedEvent += OnUcdControllerReadyToSyncStateChangedEvent;
            UcdController.OnLoggedInEvent += OnUcdControllerLoggedIn;
            UcdController.OnLoggedOutEvent += OnUcdControllerLoggedOut;

            InitializeBadgePicker();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ClearEnvironmentPickerComboBox();
            ClearBucketPickerComboBox();
            ClearCLITypePickerComboBox();

            if (UcdController != null)
            {
                UcdController.Dispose();
            }
            UcdConfigFileController = null;
            UcdConfigModel = null;
            base.OnFormClosing(e);
        }

        #endregion


        #region Button Events

        private void SelectProjectRootPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.SelectedPath;
                ProjectRootFolderTextBox.Text = path;
                UcdConfigModel.ProjectRootPath = path;
                UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
            }
        }

        private void SelectUcdPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.SelectedPath;
                UcdPathTextBox.Text = path;
                UcdConfigModel.UcdPath = path;
                UcdController.SetUcdPath(UcdConfigModel.UcdPath);
                UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
            }
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (!UcdController.IsLoggedIn)
            {
                UcdController.Login();
            }
            else
            {
                UcdController.Logout();
            }
        }

        private void SyncButton_Click(object sender, EventArgs e)
        {
            UcdController.SyncCCDContentBuild(UcdConfigModel.ProjectRootPath);
        }

        private void ClearMessagesButton_Click(object sender, EventArgs e)
        {
            ClearCLIOutputConsole();
        }

        #endregion


        #region UcdController Events

        private void OnUcdControllerOutputDataReceived(string? message)
        {
            Invoke(AppendCLIOutputMessage, message);
        }

        private void OnUcdControllerErrorDataReceived(string? message)
        {
            Invoke(AppendCLIErrorMessage, message);
        }

        private void OnUcdControllerLoggedIn()
        {
            //EnvironmentPickerComboBox.Enabled = true;
            //BucketPickerComboBox.Enabled = true;
            LoginButton.Text = "Logout";

            AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
            UcdController.SetEnvironment(UcdConfigModel.EnvironmentType);
            UcdController.SetBucket(UcdConfigModel.BucketType);
        }

        private void OnUcdControllerLoggedOut()
        {
            //EnvironmentPickerComboBox.Enabled = false;
            //BucketPickerComboBox.Enabled = false;
            LoginButton.Text = "Login";

            AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
        }

        private void OnUcdControllerReadyToSyncStateChangedEvent(bool isReadyToSync)
        {
            SyncButton.Enabled = isReadyToSync;
        }

        #endregion


        #region Environment Picker

        private void InitializeEnvironmentPickerComboBox()
        {
            ClearEnvironmentPickerComboBox();
            EnvironmentPickerComboBox.Items.AddRange(Enum.GetNames(typeof(CCDEnvironmentTypes)));
            EnvironmentPickerComboBox.SelectedIndex = (int)UcdConfigModel.EnvironmentType;
            EnvironmentPickerComboBox.SelectedIndexChanged += OnEnvironmentPickerComboBoxSelectedIndexChanged;
        }

        private void ClearEnvironmentPickerComboBox()
        {
            EnvironmentPickerComboBox.SelectedValueChanged -= OnEnvironmentPickerComboBoxSelectedIndexChanged;
            EnvironmentPickerComboBox.Items.Clear();
        }

        private void OnEnvironmentPickerComboBoxSelectedIndexChanged(object? sender, EventArgs e)
        {
            UcdConfigModel.EnvironmentType = (CCDEnvironmentTypes)EnvironmentPickerComboBox.SelectedIndex;
            AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
            UcdController.SetEnvironment(UcdConfigModel.EnvironmentType);
            UcdController.SetBucket(UcdConfigModel.BucketType);
            UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
        }

        #endregion


        #region Bucket Picker

        private void InitializeBucketPickerComboBox()
        {
            ClearBucketPickerComboBox();
            BucketPickerComboBox.Items.AddRange(Enum.GetNames(typeof(CCDBucketTypes)));
            BucketPickerComboBox.SelectedIndex = (int)UcdConfigModel.BucketType;
            BucketPickerComboBox.SelectedIndexChanged += OnBucketPickerComboBoxSelectedIndexChanged;
        }

        private void ClearBucketPickerComboBox()
        {
            BucketPickerComboBox.SelectedValueChanged -= OnBucketPickerComboBoxSelectedIndexChanged;
            BucketPickerComboBox.Items.Clear();
        }

        private void OnBucketPickerComboBoxSelectedIndexChanged(object? sender, EventArgs e)
        {
            UcdConfigModel.BucketType = (CCDBucketTypes)BucketPickerComboBox.SelectedIndex;
            AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
            UcdController.SetBucket(UcdConfigModel.BucketType);
            UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
        }

        #endregion


        #region Badge Picker

        private void InitializeBadgePicker()
        {
            if (UcdConfigModel.IsCustomBadge)
            {
                UseCustomBadgeRadioButton.Checked = true;
                AppendCLIOutputMessage("Custom Badge");
            }
            else
            {
                UseDefaultBadgeRadioButton.Checked = true;
                AppendCLIOutputMessage("Default Badge");
            }
            CustomBadgeNameTextBox.Text = UcdConfigModel.CustomBadgeName;
        }

        private void UseDefaultBadgeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (UseDefaultBadgeRadioButton.Checked)
            {
                UcdConfigModel.IsCustomBadge = false;
                SetCustomBadgePanelVisibility(false);
                UcdController.SetBadgeName(DEFAULT_BADGE_NAME);
                UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
                AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
                AppendCLIOutputMessage(string.Format("Badge name is set to \"{0}\".", DEFAULT_BADGE_NAME));
            }
        }

        private void UseCustomBadgeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (UseCustomBadgeRadioButton.Checked)
            {
                UcdConfigModel.IsCustomBadge = true;
                SetCustomBadgePanelVisibility(true);
                UcdController.SetBadgeName(UcdConfigModel.CustomBadgeName);
                UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
                AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
                AppendCLIOutputMessage(string.Format("Badge name is set to \"{0}\".", UcdConfigModel.CustomBadgeName));
            }
        }

        private void SetCustomBadgePanelVisibility(bool isVisible)
        {
            CustomBadgePanel.Visible = isVisible;
        }

        private void OnCustomBadgeNameTextBoxLeave(object sender, EventArgs e)
        {
            string newCustomBadgeName = CustomBadgeNameTextBox.Text;
            if (newCustomBadgeName != UcdConfigModel.CustomBadgeName)
            {
                UcdConfigModel.CustomBadgeName = newCustomBadgeName;
                UcdController.SetBadgeName(newCustomBadgeName);
                UcdConfigFileController.SaveUcdConfig(UcdConfigModel);

                AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
                AppendCLIOutputMessage(string.Format("Badge name is set to \"{0}\".", UcdConfigModel.CustomBadgeName));
            }
        }

        #endregion


        #region CommandLineInterface (CLI) Type Picker

        private void InitializeCLITypePickerComboBox()
        {
            ClearCLITypePickerComboBox();
            CLITypePickerComboBox.Items.AddRange(Enum.GetNames(typeof(CommandLineInterfaceTypes)));
            CLITypePickerComboBox.SelectedIndex = (int)UcdConfigModel.CLIType;
            CLITypePickerComboBox.SelectedIndexChanged += OnCLITypePickerComboBoxSelectedIndexChanged;
        }

        private void ClearCLITypePickerComboBox()
        {
            CLITypePickerComboBox.SelectedValueChanged -= OnCLITypePickerComboBoxSelectedIndexChanged;
            CLITypePickerComboBox.Items.Clear();
        }

        private void OnCLITypePickerComboBoxSelectedIndexChanged(object? sender, EventArgs e)
        {
            UcdConfigModel.CLIType = (CommandLineInterfaceTypes)CLITypePickerComboBox.SelectedIndex;
            AppendCLIOutputMessage(SEPARATOR_LINE_TEXT);
            UcdController.ChangeCommandLineInterface(UcdConfigModel.CLIType, UcdConfigModel.UcdPath);
            UcdConfigFileController.SaveUcdConfig(UcdConfigModel);
        }

        #endregion


        #region Message Utils

        private void AppendCLIOutputMessage(string? message)
        {
            CLIOutputRichTextBox.AppendText(message + "\n");
            CLIOutputRichTextBox.Focus();
            CLIOutputRichTextBox.Select(CLIOutputRichTextBox.TextLength, 0);
        }

        private void AppendCLIErrorMessage(string? message)
        {
            CLIOutputRichTextBox.SelectionStart = CLIOutputRichTextBox.TextLength;
            CLIOutputRichTextBox.SelectionLength = 0;

            CLIOutputRichTextBox.SelectionColor = ErrorMessageColor;
            CLIOutputRichTextBox.AppendText(message + "\n");
            CLIOutputRichTextBox.SelectionColor = CLIOutputRichTextBox.ForeColor;
            CLIOutputRichTextBox.Focus();
            CLIOutputRichTextBox.Select(CLIOutputRichTextBox.TextLength, 0);
        }

        private void ClearCLIOutputConsole()
        {
            CLIOutputRichTextBox.Clear();
            CLIOutputRichTextBox.Focus();
            CLIOutputRichTextBox.Select(CLIOutputRichTextBox.TextLength, 0);
        }

        #endregion

    }
}